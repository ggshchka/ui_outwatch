package webapp.js

import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import webapp.api.CoreAPI

final private class GeneratingReductionTaskComponent {

  def stepNode(s: String) =
    div(
      cls := "card",
      maxWidth := "200rem",
      div(
        cls := "card-body",
        div(
          cls := "sw-selectable card-text h3 term",
          color := "#000000",
          padding := "3px",
          textAlign := "left",
          s,
        ),
        borderColor := "#CECECE",
      ),
    )

  val node = for {
    goHandler <- Handler.create[Boolean]
    resultHandler <- Handler.create[Boolean]
    termHandler <- Handler.create[String]("")
    countOfStepsHandler <- Handler.create[String]("2")
    selectedCountOfStepsHandler <- Handler.create[String]("2")
    inputHandler <- Handler.create[String]("")
    inputFinalHandler <- Handler.create[String]("")
    openEvaluatedTermHandler <- Handler.create[Boolean]
  } yield
    div(
      cls := "row d-flex flex-column col-9 ml-2",
      header(
        cls := "row d-flex justify-content-center subsystem-title",
        marginBottom := "1.14rem",
        h1("Задание на редукцию"),
      ),
      div(
        cls := "row d-flex justify-content-center",
        div(
          cls := "form-group col-3",
          label(
            `for` := "selectCId",
            "Мин. число шагов вычисления:",
          ),
          select(
            cls := "form-control ",
            idAttr := "selectCId",
            option(selected, value := "2", "2"),
            option(value := "3", "3"),
            option(value := "4", "4"),
            option(value := "5", "5"),
            option(value := "6", "6"),
            onClick.value --> countOfStepsHandler
          ),
        ),
        button(
          "Сгенерировать задание",
          idAttr := "reductionButton",
          `type` := "submit",
          color := "#000000",
          backgroundColor := "#CECECE",
          marginTop := "30px",
          cls := "btn btn-secondary  ml-5 align-self-start",
          onClick(inputHandler).map(x => "") --> inputHandler,
          onClick.use(false) --> resultHandler,
          onClick.use(false) --> openEvaluatedTermHandler,
          onClick.use(true) --> goHandler,
          onClick(countOfStepsHandler)--> selectedCountOfStepsHandler,
          onClick(selectedCountOfStepsHandler).map(x => CoreAPI.generateTerm(x.toInt)) --> termHandler
        ),
      ),
      goHandler.map(if (_) {
        ul(
          cls := "context-wrapper content-res-wrapper",
          li(
            cls := "row d-flex justify-content-center",
            "Вычислите терм:"
          ),
          li(
            cls := "row d-flex justify-content-center term",
            termHandler.map(x => div(x)),
          ),
          hr(cls := "row d-flex justify-content-center solid"),
          li(
            cls := "row d-flex justify-content-center",
            textArea(
              `type` := "text",
              cls := "form-control mb-3 col-5 term",
              placeholder := "Ответ...",
              onInput.value --> inputHandler,
              value <-- inputHandler
            ),
          ),
          li(
            cls := "row d-flex justify-content-center",
            button(
              "Отправить",
              `type` := "submit",
              color := "#000000",
              backgroundColor := "#CECECE",
              cls := "btn btn-secondary",
              onClick(inputHandler) --> inputFinalHandler,
              onClick.use(true) --> resultHandler
            ),
          ),
        )
      } else div()),

      resultHandler.map(if (_)
        ul(
          cls := "context-wrapper content-res-wrapper",
          li(
            cls := "row d-flex justify-content-center",
            "Результат:"
          ),
          li(
            cls := "row d-flex justify-content-center term",
            inputFinalHandler.map(x => div(termHandler.map{y =>
              val evaluatedTerm = CoreAPI.evalTerm(y, "normal order")
              CoreAPI.compareTerms(x, evaluatedTerm)
            }))
          ),
          li(
            cls := "row d-flex justify-content-end",
            button(
              "Посмотреть ответ",
              `type` := "submit",
              color := "#000000",
              backgroundColor := "#CECECE",
              cls := "btn btn-secondary btn-sm mr-3",
              onClick.use(true) --> openEvaluatedTermHandler
            ),
          ),
        )
      else div()),

      openEvaluatedTermHandler.map(if (_)
          div(
            cls := "row justify-content-md-center",
            div(
              cls := "pr-1 col-9",
              termHandler.map { t =>
                  CoreAPI.evalTermWithSteps(t, "normal order").zipWithIndex.map(x =>
                    stepNode((x._2 + 1).toString + ". " + x._1),
                )
              },
            ),
        ) else div()
      ),
    )
}
