package webapp.js

object ReductionRouter {
  sealed trait Page
  case object TasksPage extends Page
  case object ContextTermPage extends Page
  case object ReductionPage extends Page
  case object GeneratorPage extends Page

  case object ExamplePage1 extends Page
  case object ExamplePage2 extends Page
  case object ExamplePage3 extends Page

  case object EmptyPage extends Page
}

