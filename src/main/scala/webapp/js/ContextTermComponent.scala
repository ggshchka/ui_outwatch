package webapp.js

import cats.effect.SyncIO
import outwatch.dsl._
import outwatch._
import outwatch.reactive.handler.Handler
import webapp.api.CoreAPI



final case class ContextTermComponent private (boolStream: Handler[String]) {
  def cntxInputField(cntx: Handler[String], resetHndl: Handler[Boolean]) = {
      input(
        cls := "term",
        `type` := "text",
        color := "",
        borderBottom := "",
        attr("animation") := "underliner_unsolved 2s steps(1) infinite",
        attr("transform") := "translateZ(0)",
        attr("will-change") := "transform, border-bottom",
        backgroundColor := "#a9b0c1",
        color := "#fff",
        //colorB.map(if (_) backgroundColor := "#228B22" else backgroundColor := "#b20000"),
        textAlign := "center",
        lineHeight := "11px",
        verticalAlign := "middle",
        padding := "2px 0px",
        marginLeft := "",
        marginRight := "",
        border := "0px solid #999",
        borderRadius := "4px",
        width := "25px",
        onInput.value --> cntx,
        resetHndl.map(
          if (_) value := "" else value := "")
      )
    }

  def getCntxWithInputFields(cntxs: List[Handler[String]], resetHndl: Handler[Boolean], contextTermList: List[Any]) = {
    var counter = -1
    contextTermList.map{
      el => if (el.isInstanceOf[String]) div(
        cls := "term",
        el.toString
      ) else {
        counter += 1
        cntxInputField(cntxs(counter), resetHndl)
      }
    }
  }

  def createCntxHandlerList(n: Int): List[Any] = {
    if (n == 0) Nil
    else None :: createCntxHandlerList(n-1)
  }

  val node: SyncIO[VNode] = for {
    goHandler <- Handler.create[Boolean]
    inputListHandler <- Handler.create[List[String]](List(""))
    boo <- Handler.create[Boolean]
    inputResetHndl <- Handler.create[Boolean]
  } yield {
    div(
      cls := "row d-flex flex-column col-9 ml-2",
      header(
        cls := "row d-flex justify-content-center subsystem-title",
        marginBottom := "1.14rem",
        h1("Заполнение пропусков"),
      ),
      button(
        "Сгенерировать терм с пропусками",
        idAttr := "reductionButton",
        `type` := "submit",
        color := "#000000",
        backgroundColor := "#CECECE",
        cls := "btn btn-lg btn-secondary col",
        onClick.use(true) --> goHandler ,
      ),
      goHandler.map(if (_) {
        val contextTerm = CoreAPI.getContextTerm(None)
        val l: List[Handler[String]] = createCntxHandlerList(contextTerm._2).map(_ => Handler.create[String].unsafeRunSync())
        ul(
          cls := "context-wrapper content-res-wrapper",
          li(
            cls := "row d-flex justify-content-center",
            getCntxWithInputFields(l, inputResetHndl, contextTerm._1),
          ),
          hr(cls := "row d-flex justify-content-center solid"),
          li(
            cls := "row d-flex justify-content-center",
            div(
              cls := "term",
              contextTerm._5
            ),
          ),
         // div(contextTerm._4.toString),
          li(
            cls := "d-flex flex-row-reverse p-1",
            button(
              "Проверить",
              idAttr := "reductionButton",
              `type` := "submit",
              color := "#000000",
              backgroundColor := "#CECECE",
              cls := "btn btn-lg btn-secondary",
              onClick(inputListHandler).map(x => List(x.head)) --> inputListHandler,
              l.map(y => y.map(z => onClick(inputListHandler).map(x => x ++ List(z)) --> inputListHandler)),
              onClick(inputListHandler).map(x => (CoreAPI.checkResOfContext(contextTerm._6, x.tail, contextTerm._5))) --> boo,
              onClick.use(true) --> inputResetHndl
              ),
          ),
          boo.map(
            if (_) div(cls := "row d-flex justify-content-center", "Правильно!")
            else div(cls := "row d-flex justify-content-center", "Ошибка!")
          )
        )
      } else div())
    )
  }
}


object ContextTermComponent {

  def init: SyncIO[ContextTermComponent] =
    for {
      boolStream <- Handler.create[String]("")
    } yield ContextTermComponent(boolStream)
}