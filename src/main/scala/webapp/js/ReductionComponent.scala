package webapp.js

import cats.effect.SyncIO
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler._
import webapp.api.CoreAPI

final private class ReductionComponent {

  def stepNode(s: String) =
    div(
      cls := "card",
      maxWidth := "50rem",
      div(
        cls := "card-body",
        div(
          cls := "sw-selectable card-text h3 term",
          color := "#000000",
          padding := "3px",
          textAlign := "left",
          s,
        ),
        borderColor := "#CECECE",
      ),
    )

  val node: SyncIO[VNode] = for {
    term     <- Handler.create[String]
    redStrat <- Handler.create[String]("normal order")
    submits  <- Handler.create[String]
  } yield
    div(
      cls := "d-flex flex-column col-9",
      header(
        cls := "row d-flex justify-content-center subsystem-title",
        marginBottom := "1.14rem",
        h1("Вычисление терма"),
      ),
      div(
        cls := "reduction-wrapper content-res-wrapper",
        div(
          cls := "row d-flex justify-content-center",
          div(
            cls := "col col-md-6 align-self-end",
            label(`for` := "termInputId", "Введите терм"),
            input(
              `type` := "text",
              cls := "form-control mb-3",
              idAttr := "termInputId",
              placeholder := "\\x.x",
              onInput.value --> term,
            ),
          ),
          div(
            cls := "col col-md-3 align-self-end",
            label(`for` := "redStrategyId", "Стратегия редукции"),
            select(
              cls := "form-control mb-3",
              idAttr := "redStrategyId",
              option(selected, value := "normal order", "normal order"),
              option(value := "application order", "application order"),
              option(value := "call-by-name", "call-by-name"),
              option(value := "call-by-value", "call-by-value"),
              onChange.value --> redStrat,
            ),
          ),
          div(
            cls := "col col-md-3 align-self-end",
            button(
              "Вычислить",
              idAttr := "reductionButton",
              `type` := "submit",
              color := "#000000",
              backgroundColor := "#CECECE",
              cls := "btn btn-secondary mb-3",
              onClick(term) --> submits,
            ),

          ),
        ),
        div(
          cls := "row justify-content-md-left",
          div(
            cls := "pr-1 col-9",
            submits.map { t =>
              redStrat.map(s =>
                CoreAPI
                  .evalTermWithSteps(t, s).zipWithIndex.map(x =>
                  stepNode((x._2 + 1).toString + ". " + x._1),
                ),
              )
            },
          ),
        ),
      ),
    )
}
