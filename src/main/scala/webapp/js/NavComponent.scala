package webapp.js

import cats.effect.{IO, SyncIO}
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import webapp.js.ReductionRouter._

final private class NavComponent (pageHndl: Handler[Page]) {

//  def listGroupItem(name: String, page: Page) = a(
//    cls := "list-group-item list-group-item-action",
//    href := "#",
//    name,
//    onClick(page) --> pageHndl
//  )
//
//  val node = SyncIO(
//    div(
//      cls := "border-right",
//      idAttr := "sidebar-wrapper",
//      div(
//        cls := "sidebar-heading",
//        color := "aliceblue",
//        backgroundColor := "#1E2229",
//
//        "Модели вычислений",
//      ),
//      div(
//        cls := "list-group list-group-flush",
//        listGroupItem("Система редукции лямбда-термов", TasksPage),
//        listGroupItem("Система №2 ...", ExamplePage1),
//        listGroupItem("Система №3 ...", ExamplePage2),
//        listGroupItem("Система №4 ...", ExamplePage3),
//      ),
//    )
//  )



    def listGroupItem(name: String, page: Page) = li(
    cls := "nav-item",
    a(
      cls := "nav-link text-dark font-italic bg-light",
      href := "#",
      name,
      onClick(page) --> pageHndl
    )
  )

  val node = SyncIO(
      div(
        cls := "vertical-nav bg-light",
        idAttr := "sidebar",
        div(
          cls:= "py-4 px-3 mb-4",
          backgroundColor := "#B9BFCD",
          div(
            cls := "d-flex align-items-center",
            img(
              src := "img/logo.png",
              alt := "...",
              width := "50px",
              height := "auto",
              cls := "mr-3 rounded-circle img-thumbnail shadow-sm",
            ),
            div(
              cls := "media-body",
              h5(cls := "m-0","Модели вычислений"),
            )
          )
        ),
        ul(
          cls := "nav flex-column bg-white mb-0",
          listGroupItem("Система редукции лямбда-термов", TasksPage),
          listGroupItem("Система №2 ...", ExamplePage1),
          listGroupItem("Система №3 ...", ExamplePage2),
          listGroupItem("Система №4 ...", ExamplePage3),
        ),

    )
  )
}
