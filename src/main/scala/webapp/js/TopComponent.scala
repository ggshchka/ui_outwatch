package webapp.js

import cats.effect.IO
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import webapp.js.ReductionRouter._
import webapp.js.TopComponent._

final case class TopComponent(
  headerComponent: HeaderComponent,
  navComponent: VNode, //NavComponent,
  tasksComponent: VNode, //TasksComponent,
  contextTermComponent: ContextTermComponent,
  reductionComponent: VNode,
  generatorComponent: VNode,
) {

  def node =
      VDomModifier(
        div(
          cls := "d-flex",
          headerComponent.toggleStream.map{
            case true => div(

              navComponent,
            )
            case _ => div()
          },
          div(
            cls := "page-content",
            idAttr := "content",
            headerComponent.node,
            pgeHndl.map{
              case ReductionPage => reductionComponent
              case GeneratorPage => generatorComponent
              case ContextTermPage => contextTermComponent.node.unsafeRunSync()
              case TasksPage    => tasksComponent
              //              case ExamplePage1 => generatorComponent
              //              case ExamplePage2 => generatorComponent
              //              case ExamplePage3 => generatorComponent
            }
          ),
        ),

      )
}

object TopComponent {
  val pgeHndl = Handler.unsafe[Page](TasksPage)
  def init: IO[TopComponent] =(
    for {
      header                    <- HeaderComponent.init
      navComponent              <- new NavComponent(pgeHndl).node
      tasksComponent            <- new TasksComponent(pgeHndl).node
      contextTermComponent      <- ContextTermComponent.init
      reductionComponent        <- new ReductionComponent().node
      generatorComponent        <- new GeneratingReductionTaskComponent().node
    } yield
      TopComponent(
        header,
        navComponent,
        tasksComponent,
        contextTermComponent,
        reductionComponent,
        generatorComponent,
      )).toIO
}
